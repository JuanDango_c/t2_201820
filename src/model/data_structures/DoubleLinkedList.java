package model.data_structures;

import java.util.Iterator;

/**
 * This class creates a doubly linked list of the objects that enters as parameters
 * @param <T> the type of object that enters as parameter
 */
public class DoubleLinkedList<T extends Comparable <T>> implements DoublyLinkedList<T> {

	/**
	 * Represents one node of the linked list 
	 * @param <T> type of object
	 */
	private class Node<T extends Comparable<T>>{
		
		/**
		 * Object inside the node
		 */
		private T object;
		
		/**
		 * Previous node in the linked list
		 */
		private Node<T> previousNode;
		
		/**
		 * Next node in the linked list
		 */
		private Node<T> nextNode;
		
		/**
		 * It creates a node with the object in the parameter
		 * @param object object inside the list. object != null
		 */
		public Node(T object)
		{
			this.object = object;
		}
		
		/**
		 * It returns the previous node
		 * @return previous node in the list
		 */
		public Node<T> getPrevious()
		{
			return previousNode;
		}
		
		/**
		 * It returns the next node in the list
		 * @return next node in the list
		 */
		public Node<T> getNext()
		{
			return nextNode;
		}
		
		/**
		 * Returns the object in the node
		 * @return object in the node
		 */
		public T getObject()
		{
			return object;
		}
		
		/**
		 * It changes the next node by the one in the parameter
		 * @param obj node that is going to become the next
		 */
		public void changeNext(Node<T> obj)
		{
			nextNode = obj;
		}
		
		/**
		 * It changes the previous node by the one in the parameter
		 * @param obj node that is going to become the previous
		 */
		public void changePrevious(Node<T> obj)
		{
			previousNode = obj;
		}
	}
	
	/**
	 * it represents the first element in the array
	 */
	private Node<T> First;
	
	/**
	 * the amount of elements in the list
	 */
	private int size;
	
	
	protected class MyIterator<T extends Comparable<T>> implements Iterator<T>
	{
		private DoubleLinkedList<T>.Node<T> actual;
		
		private int cont;
		
		public MyIterator()
		{
			actual = (DoubleLinkedList<T>.Node<T>) First;
			cont = 0;
		}
		@Override
		public boolean hasNext() {
			if(actual!= null)
			{
				return actual.getNext()!=null || cont == 0;
			}
			return false;
		}

		@Override
		public T next(){
			if(cont == 0)
			{
				cont ++;
			}else
			{
				actual = actual.getNext();
			}
			return actual.getObject();
		}

		
	}
	
	public DoubleLinkedList() 
	{
		size = 0;
		First = null;
	}

	@Override
	public Iterator<T> iterator() {
		return new MyIterator<T>();
	}

	@Override
	public Integer getSize() {
		return size;
	}

	@Override
	public T getFirst() {
		return First.getObject();
	}

	@Override
	public T getObject(int posicion) {
		Node<T> n = First;
		for (int i = 1; i <= posicion; i++) {
			n = n.getNext();
		}
		return n.getObject();
	}

	@Override
	public void add(T aAgregar) {
		Node<T> n = First;
		if(First == null)
		{
			First = new Node<T>(aAgregar);
			size ++;
			return;
		}
		else
		{
			while(n.getNext()!=null)
			{
				n = n.getNext();
			}
			Node<T> a = new Node<T>(aAgregar);
			a.changePrevious(n);
			n.changeNext(a);
			size ++;
		}
	}

	@Override
	public void addAt(T aAgregar, int posicion) {
		Node<T> actual = First;
		Node<T> agr = new Node<T>(aAgregar);
		if(posicion == 0)
		{
			First = new Node<T>(aAgregar);
			First.changeNext(actual);
			size ++;
		}else if(posicion == size)
		{
			add(aAgregar);
			return;
		}else if(posicion < size)
		{
			int i=1;
			while (i <= posicion) {
				actual = actual.getNext();
				i++;
			}
			Node<T> ant = actual.getPrevious();
			agr.changePrevious(ant);
			agr.changeNext(actual);
			ant.changeNext(agr);
			actual.changePrevious(agr);
			size ++;
		}

	}

	@Override
	public void delete(T aEliminar) throws Exception{
		Node<T> aElim = First;
		if(First.getObject().equals(aEliminar))
		{
			First = aElim.getNext();
			aElim.changePrevious(null);
			size--;
		}else
		{
			boolean b = false;
			while(aElim != null && !b)
			{
				aElim = aElim.getNext();
				b = aElim.getObject().equals(aEliminar)? true:false;
			}
			if(aElim == null)
			{
				throw new Exception ("Error");
			}
				Node<T> ant = aElim.getPrevious();
				Node<T> sig = aElim.getNext();
				ant.changeNext(sig);
				if(sig != null)
				{
					sig.changePrevious(ant);
				}
				size--;
		}
	}

	@Override
	public void deleteAt(int posicion) throws Exception {
		Node<T> act = First;
		if(posicion >= size && posicion < 0)
		{
			throw new Exception ("Introducir una posicion que se encuentre en la lista");
		}
		if(posicion == 0)
		{
			First = First.getNext();
			First.changePrevious(null);
			size--;
		}
		else
		{
			for (int i = 1; i <= posicion; i++) {
				act = act.getNext();
			}
			Node<T> ant = act.getPrevious();
			if(act.getNext()!=null)
			{
				act.getNext().changePrevious(ant);
			}
			ant.changeNext(act.getNext());
			size--;
		}
	}
}
