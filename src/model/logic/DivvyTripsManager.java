package model.logic;

import api.IDivvyTripsManager;
import model.vo.VOByke;
import model.vo.VOStations;
import model.vo.VOTrip;
import model.data_structures.DoubleLinkedList;
import model.data_structures.DoublyLinkedList;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Iterator;

public class DivvyTripsManager implements IDivvyTripsManager {

	public DoublyLinkedList<VOTrip> lista;
	public DoublyLinkedList<VOByke> listaB;
	public DoublyLinkedList<VOStations> listaStations;
	
	public void loadStations (String stationsFile) {
		try {
			listaStations = new DoubleLinkedList<>();
			FileReader lector = new FileReader(stationsFile);
			BufferedReader bfLector = new BufferedReader(lector);
			String linea = bfLector.readLine();
			linea = bfLector.readLine();
			while(linea != null)
			{
				String[] separado = linea.split(",");
				VOStations v = new VOStations(Integer.parseInt(separado[0]), Integer.parseInt(separado[5]), separado[1]);
				listaStations.addAt(v, 0);
				linea = bfLector.readLine();
			}
			
			bfLector.close();
			lector.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	public void loadTrips (String tripsFile) {
		try {
			lista = new DoubleLinkedList<>();
			listaB = new DoubleLinkedList<>();
			FileReader lector = new FileReader(tripsFile);
			BufferedReader bfLector = new BufferedReader(lector);
			String linea = bfLector.readLine();
			linea = bfLector.readLine();
			System.out.println(linea);
			while(linea != null)
			{
				String[] separado = linea.split(",");
				VOByke k = new VOByke(Integer.parseInt(separado[3]));
				String gender = separado.length == 10? "": separado[10].toLowerCase();
				VOTrip v = new VOTrip(Integer.parseInt(separado[0]), Integer.parseInt(separado[4]), separado[6], separado[8], gender);
				lista.addAt(v, 0);
				listaB.addAt(k, 0);
				linea = bfLector.readLine();
			}
			//System.out.println(lista.getObject(9).getGender());
			bfLector.close();
			lector.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public DoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		String g = gender.toLowerCase();
		DoublyLinkedList<VOTrip> trip = new DoubleLinkedList<>();
		Iterator<VOTrip> iter = lista.iterator();
		while(iter.hasNext())
		{
			VOTrip v = iter.next();
			if(v.getGender().equals(g))
			{
				trip.addAt(v, 0);
			}
		}
		return trip;
	}

	@Override
	public DoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		VOStations estacion = null;
		Iterator<VOStations> iterS = listaStations.iterator();
		while(iterS.hasNext() && estacion == null)
		{
			VOStations s = iterS.next();
			if(s.id() == stationID)
			{
				estacion = s;
			}
		}
		System.out.println("--------------");
		DoublyLinkedList<VOTrip> trip = new DoubleLinkedList<>();
		if(estacion != null)
		{
			Iterator<VOTrip> iterT = lista.iterator();
			while(iterT.hasNext())
			{
				VOTrip viaje = iterT.next();
				if(viaje.getToStation().equals(estacion.getName()))
				{
					trip.addAt(viaje, 0);
				}
			}
		}
		return trip;
	}	


}
