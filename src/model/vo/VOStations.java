package model.vo;

/**
 * Representation of a station object
 */
public class VOStations implements Comparable {

	/**
	 * Id del viaje
	 */
	private int id;
	
	/**
	 * capacidad de la estacion
	 */
	private int dCapacity;
	
	/**
	 * Nombre de la estacion
	 */
	private String name;
	
	
	/**
	 * Crea una nueva estacion con los valores que entran por parametro
	 * @param id id de la estacion
	 * @param dCapacity capacidad de la estacion
	 * @param name nombre de la estacion
	 */
	public VOStations (int id, int dCapacity, String name)
	{
		this.id = id;
		this.dCapacity = dCapacity;
		this.name = name;
	}
	/**
	 * @return id - Station_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - capacity.
	 */
	public double getdCapacity() {
		return dCapacity;
	}

	/**
	 * @return station_name - name of the station .
	 */
	public String getName() {
		return name;
	}

	@Override
	public int compareTo(Object o) {
		return 0;
	}
}
