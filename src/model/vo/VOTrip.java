package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip implements Comparable {

	/**
	 * Id del viaje
	 */
	private int id;
	
	/**
	 * Duracion del viaje en segundos
	 */
	private int tripInSeconds;
	
	/**
	 * Estacion desde la cual salio la bicicleta
	 */
	private String fromStation;
	
	/**
	 * Estacion en la cual termino el viaje
	 */
	private String toStation;
	
	/**
	 * 
	 */
	private String gender;
	
	/**
	 * Crea una nueva bicicleta con los valores que entran por parametro
	 * @param id
	 * @param tripInSeconds
	 * @param fromStation
	 * @param toStation
	 */
	public VOTrip (int id, int tripInSeconds, String fromStation, String toStation, String gender)
	{
		this.id = id;
		this.tripInSeconds = tripInSeconds;
		this.fromStation = fromStation;
		this.toStation = toStation;
		this.gender = gender;
	}
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return id;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripInSeconds;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		// TODO Auto-generated method stub
		return toStation;
	}

	/**
	 * @return gender  .
	 */
	public String getGender() {
		// TODO Auto-generated method stub
		return gender;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
}
